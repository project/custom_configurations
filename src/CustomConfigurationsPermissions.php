<?php

namespace Drupal\custom_configurations;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Dynamic permissions class for Custom Configurations.
 */
class CustomConfigurationsPermissions {

  use StringTranslationTrait;

  /**
   * Define permissions.
   */
  public function permissions() {
    $permissions = [];

    foreach (\Drupal::service('custom_configurations.manager')->getConfigPlugins() as $plugin) {
      $permissions += [
        'access to ' . $plugin['id']  . ' custom configuration' => [
          'title' => $this->t('Access to %title custom configuration', array('%title' => $plugin['title'])),
        ]
      ];
    }

    return $permissions;
  }

}

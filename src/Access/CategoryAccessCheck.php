<?php

namespace Drupal\custom_configurations\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Symfony\Component\Routing\Route;

/**
 * Checks access for displaying configuration page.
 */
class CategoryAccessCheck implements AccessInterface {

  /**
   * A custom access check.
   *
   * @param \Symfony\Component\Routing\Route $route
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $route_parts = explode('.', $route_match->getRouteName());

    if ((int) $account->id() === 1) {
      return AccessResult::allowed();
    }

    if (!empty($route_parts[2]) && $route_parts[2] === 'category') {
      $category_id = $route_parts[1];
    }
    else {
      return AccessResult::neutral();
    }

    foreach (\Drupal::service('custom_configurations.manager')->getConfigPlugins() as $plugin) {
      if (
        $account->hasPermission('access to ' . $plugin['id']  . ' custom configuration') &&
        isset($category_id) &&
        isset($plugin['category_id']) &&
        $category_id === $plugin['category_id']
      ) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::forbidden();
  }

}

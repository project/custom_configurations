<?php

namespace Drupal\custom_configurations\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\ProxyClass\Routing\RouteBuilder;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\RouteProvider;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\custom_configurations\CustomConfigurationsManager;
use Symfony\Component\Routing\Route;

/**
 * Class CustomConfigurationsController.
 *
 * @package Drupal\custom_configurations\Controller
 */
class CustomConfigurationsController extends ControllerBase {

  /**
   * Drupal\custom_configurations\CustomConfigurationsManager definition.
   *
   * @var \Drupal\custom_configurations\CustomConfigurationsManager
   */
  protected $customConfigurationsManager;

  /**
   * The route builder.
   *
   * @var \Drupal\Core\ProxyClass\Routing\RouteBuilder
   */
  protected $routerBuilder;

  /**
   * The route builder.
   *
   * @var \Drupal\Core\Routing\RouteProvider
   */
  protected $routerProvider;

  /**
   * {@inheritdoc}
   */
  public function __construct(CustomConfigurationsManager $custom_configurations_manager, RouteBuilder $router_builder, RouteProvider $router_provider) {
    $this->customConfigurationsManager = $custom_configurations_manager;
    $this->routerBuilder = $router_builder;
    $this->routerProvider = $router_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('custom_configurations.manager'),
      $container->get('router.builder'),
      $container->get('router.route_provider')
    );
  }

  /**
   * Access to the custom configuration overview page.
   *
   * @param \Symfony\Component\Routing\Route $route
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {

    if ((int) $account->id() === 1) {
      return AccessResult::allowed();
    }

    foreach (\Drupal::service('custom_configurations.manager')->getConfigPlugins() as $plugin) {
      if ($account->hasPermission('access to ' . $plugin['id']  . ' custom configuration')) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::forbidden();
  }

  /**
   * Returns index custom configurations plugin page.
   */
  public function getIndex() {
    $user = \Drupal::currentUser();
    $plugins = $this->customConfigurationsManager->getConfigPlugins();

    // Sort plugins by title.
    uasort($plugins, function ($a, $b) {
      if ($a['title'] == $b['title']) {
        return 0;
      }
      return ($a['title'] < $b['title']) ? -1 : 1;
    });

    if (!empty($plugins)) {
      $content = [];

      foreach ($plugins as $plugin) {
        if ($user->hasPermission('access to ' . $plugin['id']  . ' custom configuration') === FALSE) {
          continue;
        }

        $route = 'custom_configurations.' . $plugin['id'] . '.form';
        if (empty($this->routerProvider->getRoutesByNames([$route]))) {
          $this->routerBuilder->rebuild();
        }
        $content[$plugin['id']] = [
          'title' => $plugin['title'],
          'description' => $plugin['description'] ?? '',
          'url' => Url::fromRoute($route, ['plugin_id' => $plugin['id']]),
        ];
      }
      $build = [
        '#theme' => 'admin_block_content',
        '#content' => $content,
      ];
    }
    else {
      $path = 'custom_configurations/src/Plugin/CustomConfigurations/ExampleConfigPlugin.php';
      $build = ['#markup' => $this->t('No active plugins found. To define your custom plugin you can use an example placed in %path', ['%path' => $path])];
    }

    return $build;
  }

}
